<?php
	header('refresh:10; url=index.php');
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');
	session_start();
	require_once dirname(__FILE__).'/GoogleClientApi/Google_Client.php';
	require_once dirname(__FILE__).'/GoogleClientApi/contrib/Google_AnalyticsService.php';

	$client = new Google_Client();
	$client->setApplicationName("Google Analytics PHP Starter Application");

	$client->setClientId('844862102861-r2stcrjctrh5q1htq7j6u721842ochj7.apps.googleusercontent.com');
	$client->setClientSecret('cxAi1x_8DorvBVpS1b_Pwfrd');
	$client->setRedirectUri('http://localhost/sandbox/googleanalytics/index.php');
	// $client->setDeveloperKey('insert_your_developer_key');
	$service = new Google_AnalyticsService($client);

	if (isset($_GET['logout'])) {
		unset($_SESSION['token']);
	}

	if (isset($_GET['code'])) {
		$client->authenticate();
		$_SESSION['token'] = $client->getAccessToken();
		$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
		header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
	}

	if (isset($_SESSION['token'])) {
		$client->setAccessToken($_SESSION['token']);
	}

	if (!$client->getAccessToken()) { // auth call to google
			$authUrl = $client->createAuthUrl();
			header("Location: ".$authUrl);
			die;
	}



	//specific project
	$projectId = '63630912'; //see here for how to get your project id: http://goo.gl/Tpcgc

	// metrics
	$_params[] = 'date';
	$_params[] = 'date_year';
	$_params[] = 'date_month';
	$_params[] = 'date_day';
	// dimensions
	$_params[] = 'visits';
	$_params[] = 'pageviews';
	$_params[] = 'avgTimeOnSite';
	$_params[] = 'bounces';

	//for more metrics see here: http://goo.gl/Tpcgc

	// $from = date('Y-m-d', time()-2*24*60*60); // 2 days ago
	// $to = date('Y-m-d'); // today
	$from = date('Y-m-d', time()-7*24*60*60); // 7 days ago
	$to = date('Y-m-d', time()); // 1 days ago
	// $from = date('Y-m-d');
	// $to = date('Y-m-d');

	$metrics = 'ga:visits,ga:pageviews,ga:avgTimeOnSite,ga:bounces,ga:entranceBounceRate,ga:visitBounceRate';
	$dimensions = 'ga:date,ga:year,ga:month,ga:day';
	$data = $service->data_ga->get('ga:'.$projectId, $from, $to, $metrics, array('dimensions' => $dimensions));

	$retData = array();
	foreach($data['rows'] as $row)
	{
		 $dataRow = array();
		 foreach($_params as $colNr => $column)
		 {
				 $dataRow[$column] = $row[$colNr];
		 }
		 array_push($retData, $dataRow);
	}


	//Databse connect
	$link = mysqli_connect('79.170.40.230', 'web230-dbserver', 'oak3n7u11', 'web230-dbserver');
	if (!$link) die('Could not connect: ' . mysql_error());

	foreach ($retData as $key => $row) {

		$result = mysqli_query($link,'
			SELECT date
			FROM tbl_traffic
			WHERE date = "'. $row['date_year'] .'-' . $row['date_month'] . '-' . $row['date_day'] . '"
		');
		$row1 = mysqli_fetch_row($result);

		if($row1[0] != null){
			mysqli_query($link,'
				UPDATE tbl_traffic
				SET visits = "'. $row['visits'] .'", pageviews = "'. $row['pageviews'] .'", avg_time_site = "'. $row['avgTimeOnSite'] .'", bounce_rate = "'. $row['bounces'] .'"
				WHERE date = "'.$row1[0].'"
			');	
		}else{
			mysqli_query($link,'
				INSERT INTO tbl_traffic (visits, pageviews, date, avg_time_site, bounce_rate)
				VALUES ("'. $row['visits'] .'","'. $row['pageviews'] .'","'. $row['date_year'] .'-' . $row['date_month'] . '-' . $row['date_day'] . '","'. $row['avgTimeOnSite'] .'","'. $row['bounces'] .'")
			');			
		}
	}

	mysqli_close($link);

	$records = 'updated';

	echo $_GET['callback'] . '(' . json_encode($records) . ');';


	