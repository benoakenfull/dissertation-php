<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: User Management class
*	Date: 7 Mar 2013
* ------------------------------------- */

class User extends Controller {
	
	function index(){

		//Get POST data & cleans
		@$username = mysql_real_escape_string($_POST['username']);
		@$password = mysql_real_escape_string($_POST['password']);

		//Conver password to SHA1
		$password = sha1($password);

		//Loading model class
		$loadModel = $this->loadModel('user_model');

		//Loading relative method & pass data
		$user = $loadModel->userCheck($username, $password);

		//Loop through database data & assign $
		foreach ($user as $key) {
			$dbusername = $key->username;
			$dbpassword = $key->password;
			$dbuserid = $key->id;
		}
		
		//If user array empty
		if(empty($user)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($user)){
			$return['error'] = false;
			$return['user'] = $dbuserid;
			$return['url'] = 'dashboard.html';
			echo json_encode($return);
		}

	}
    
}
