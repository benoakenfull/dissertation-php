<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Website traffic class
*	Date: 15 Apr 2013
* ------------------------------------- */

class Traffic extends Controller {

	function index(){
 	
	}

	function gettrafficdata(){

		//Loading model class
		$loadModel = $this->loadModel('traffic_model');	

		$gettraffic = $loadModel->getTraffic();
	 	
		//If user array empty
		if(empty($gettraffic)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($gettraffic)){
			echo json_encode($gettraffic);
		}  
	}

	function gettodaytraffic(){

		//Loading model class
		$loadModel = $this->loadModel('traffic_model');	

		$gettraffic = $loadModel->getLatestTraffic();
	 	
		//If user array empty
		if(empty($gettraffic)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($gettraffic)){
			echo json_encode($gettraffic);
		}  
	}
}