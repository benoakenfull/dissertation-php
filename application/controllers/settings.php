<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Product Management class
*	Date: 7 Mar 2013
* ------------------------------------- */

class Settings extends Controller {
	
	//Return list of all products
	function index(){

		//Loading model class
		$loadModel = $this->loadModel('settings_model');

		//Loading relative method & pass data
		$settings = $loadModel->getSettings();
		
		//If user array empty
		if(empty($settings)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($settings)){
			echo json_encode($settings);
		};

	}

	//Update settings on site
	function updatesettings(){

		//Load vairables and clean
    	@$userid = mysql_real_escape_string($_POST['userid']);
    	@$username = mysql_real_escape_string($_POST['username']);
    	@$ganalyticsid = mysql_real_escape_string($_POST['ganalyticsid']);
		
		//Loading model class
		$loadModel = $this->loadModel('settings_model');

		//Loading relative method & pass data
		$settings = $loadModel->updateSettings($userid, $username, $ganalyticsid);

		//If user array empty
		if(empty($settings)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($settings)){
			$return['error'] = false;
			$return['msg'] = 'updated';
			echo json_encode($return);
		};			

	}

}