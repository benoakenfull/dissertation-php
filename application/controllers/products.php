<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Product Management class
*	Date: 7 Mar 2013
* ------------------------------------- */

class Products extends Controller {
	
	//Return list of all products
	function index(){

		//Loading model class
		$loadModel = $this->loadModel('product_model');

		//Loading relative method & pass data
		$products = $loadModel->getProducts();
		
		//If user array empty
		if(empty($products)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($products)){
			echo json_encode($products);
		};

	}
    
    //Return single product
    function getproduct(){

    	//Get variables
    	@$product_id = $username = mysql_real_escape_string($_POST['productid']);

 		//Loading model class
		$loadModel = $this->loadModel('product_model');

		//Loading relative method & pass data
		$product = $loadModel->getSingleProduct($product_id);
		
		//If user array empty
		if(empty($product)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($product)){
			echo json_encode($product);
		}   	
    }

    //Update single product
    function addvariance(){

    	//Get variables
    	@$product_id = mysql_real_escape_string($_POST['productid']);
    	@$size = mysql_real_escape_string($_POST['size']);
    	@$stock = mysql_real_escape_string($_POST['stock']);
    	@$price = mysql_real_escape_string($_POST['price']);

    	//Check that data is passed
    	if($product_id != null){
	  		//Loading model class
			$loadModel = $this->loadModel('product_model');

			//Loading relative method & pass data
			$variance = $loadModel->addVariance($product_id, $size, $stock, $price);

			//Get changed data
			// $newvariance = $loadModel->getSingleProduct($product_id);	
    	}
		
		//If user array empty
		if(empty($variance)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($variance)){
			echo json_encode($variance);
		}   	
    }

    //Update single product
    function updateVairant(){

    	//Get variables
    	@$product_id = mysql_real_escape_string($_POST['productid']);
    	@$detail_id = mysql_real_escape_string($_POST['detailid']);
    	@$size = mysql_real_escape_string($_POST['size']);
    	@$stock = mysql_real_escape_string($_POST['stock']);
    	@$price = mysql_real_escape_string($_POST['price']);

    	//Check that data is passed
    	if($detail_id != null){
	  		//Loading model class
			$loadModel = $this->loadModel('product_model');

			//Loading relative method & pass data
			$variance = $loadModel->updateVariance($detail_id, $size, $stock, $price);

			//Get changed data
			$newvariance = $loadModel->getSingleProduct($product_id);	
    	}
		
		//If user array empty
		if(empty($newvariance)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($newvariance)){
			echo json_encode($newvariance);
		}   	
    }

    //Delete Variance
    function deletevariance(){

 		//Get variables
    	@$detail_id = $username = mysql_real_escape_string($_POST['detailid']);

    	//Check that data is passed
    	if($detail_id != null){
	  		//Loading model class
			$loadModel = $this->loadModel('product_model');

			//Loading relative method & pass data
			$variance = $loadModel->deleteVariance($detail_id);   		
    	}
    	
    	//If user array empty
		if(empty($variance)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($variance)){
			$return['error'] = false;
			echo json_encode($return);
		}  
    }
}
