<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Third Party Stock
*	Date: 22 Apr 2013
* ------------------------------------- */

class External_stock extends Controller {
	
	//Return list of all products
	function index(){


	}

	function etsy(){
		
		//Search Params
		@$params = mysql_real_escape_string($_POST['search']);

		$params = str_replace(' ', '%20', $params);

		// $params = "Quartz%20Crystal%20Bullet%20Necklace";
		$apikey = "7r9czbdmdolsxoqernx0qrpo";


		$apiurl = "http://openapi.etsy.com/v2/listings/active?fields=listing_id,title,price,stock,quantity,has_variations&limit=1&api_key=".$apikey ."&keywords=".$params;
		// $apiurl = "http://openapi.etsy.com/v2/listings/active?api_key=".$apikey."&limit=28&keywords=jewelry";		
	
		$products = array();
		$results = json_decode(file_get_contents($apiurl));
		
		$i = 0;

		foreach($results->results as $product){
			$products[$i]["pid"]      = $product->listing_id;
			$products[$i]["title"]    = $product->title;
			$products[$i]["price"]    = $product->price;
			$products[$i]["quantity"] = $product->quantity;
			$products[$i]["has_variations"] = $product->has_variations;
			
			$i++;
		}
		
		$jsonified = json_encode($products);
		
		$jsoncache = "cache.json";
		$fp = fopen($jsoncache, "w");
		fwrite($fp, $jsonified);
		fclose($fp);
		

		echo json_encode($products);
	}
}