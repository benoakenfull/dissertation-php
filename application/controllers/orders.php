<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Orders Management class
*	Date: 29 Mar 2013
* ------------------------------------- */

class Orders extends Controller {
	
	//Return list of all products
	function index(){

		//Loading model class
		$loadModel = $this->loadModel('order_model');

		//Loading relative method & pass data
		$orders = $loadModel->getOrders();
		
		//If user array empty
		if(empty($orders)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($orders)){
			echo json_encode($orders);
		};

	}

	//Return single order
	function getorderdetails(){

    	//Get variables
    	@$orderid = mysql_real_escape_string($_POST['orderid']);

 		//Loading model class
		$loadModel = $this->loadModel('order_model');

		//Loading relative method & pass data
		$order = $loadModel->getSingleOrderDetails($orderid);
		
		//If user array empty
		if(empty($order)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($order)){
			echo json_encode($order);
		}   

	}

	function getorder(){

    	//Get variables
    	@$orderid = mysql_real_escape_string($_POST['orderid']);

 		//Loading model class
		$loadModel = $this->loadModel('order_model');

		//Loading relative method & pass data
		$order = $loadModel->getSingleOrder($orderid);
		
		//If user array empty
		if(empty($order)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($order)){
			echo json_encode($order);
		}   		
	}

}
