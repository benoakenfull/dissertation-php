<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Dashboard class
*	Date: 1 Apr 2013
* ------------------------------------- */

class Dashboard extends Controller {
	
	//Return dashboard info
	function index(){

		//Loading model class
		$loadModel = $this->loadModel('dashboard_model');

		//Loading relative method & pass data
		$dashboard = $loadModel->getDashboard();
		
		//If user array empty
		if(empty($dashboard)){
			$return['error'] = true;
			$return['msg'] = 'fail';
			echo json_encode($return);

		//Else if user array has data
		}else if(!empty($dashboard)){
			echo json_encode($dashboard);
		};
		
	}

}
