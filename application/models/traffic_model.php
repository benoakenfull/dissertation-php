<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Product Management Model
*	Date: 7 Mar 2013
* ------------------------------------- */

class Traffic_model extends Model {

	/* ------------------------
	*	Add traffic to DB
	* ------------------------ */
	public function addTraffic($retData){


		foreach ($retData as $key => $row) {
			$result = $this->execute('
				INSERT INTO tbl_traffic (visits, pageviews, date, avg_time_site, bounce_rate, type)
				VALUES ("'. $row['visits'] .'","'. $row['pageviews'] .'","'. $row['date_year'] .'-' . $row['date_month'] . '-' . $row['date_day'] . '","'. $row['avgTimeOnSite'] .'","'. $row['bounces'] .'","7-day")
			');
		}

		return $result;

	}

	public function getTraffic(){

		$result = $this->query('
			SELECT SUM(visits) as visits , SUM(pageviews) as pageviews, DATE_FORMAT(DATE(date),"%e %b %Y") AS date, TRUNCATE(AVG(avg_time_site), 1) as avg_site, TRUNCATE((SUM(bounce_rate))/(SUM(visits)) * 100, 1) AS bounce_rate  
			FROM tbl_traffic
			WHERE date > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)
			ORDER BY date DESC
		');
		
		return $result;
	}

	public function getLatestTraffic(){

		$result = $this->query('
			SELECT visits, pageviews, date, avg_time_site, bounce_rate 
			FROM tbl_traffic
			WHERE date = CURDATE()
		');
		
		return $result;
	}

}