<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Order Management Model
*	Date: 29 Mar 2013
* ------------------------------------- */

class Order_model extends Model {

	/* ------------------------
	*	Get Orders
	* ------------------------ */
	public function getOrders(){

		$result = $this->query('
			SELECT c.name, c.surname, o.order_id, o.order_amount, o.fulfilled
			FROM tbl_customers c
			RIGHT JOIN tbl_orders o 
				ON c.customer_id = o.customer_id
			');
		return $result;

	}

	/* ------------------------
	*	Get Single Order Details
	* ------------------------ */
	public function getSingleOrderDetails($order_id){

		$result = $this->query('
			SELECT o.order_id, od.price, p.name, od.qty, od.size
			FROM tbl_orders o
			LEFT JOIN tbl_order_details od
				ON o.order_id = od.order_id
			LEFT JOIN tbl_products p
				ON od.product_id = p.product_id
			WHERE o.order_id = "'. $order_id .'"
			');

		return $result; 
	}

	/* ------------------------
	*	Get Single Order Address
	* ------------------------ */
	public function getSingleOrder($order_id){

		$result = $this->query('
			SELECT c.name, c.surname, c.street, c.city, o.order_id, o.order_amount, o.postage_amt, o.postage_type, o.discount_amt, o.discount_type, o.total_amount
			FROM tbl_customers c
			RIGHT JOIN tbl_orders o 
				ON c.customer_id = o.customer_id
			WHERE o.order_id = "'. $order_id .'"
			');

		return $result;
	}
}