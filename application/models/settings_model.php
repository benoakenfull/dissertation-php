<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Settings Managemet Model
*	Date: 19 APR 2013
* ------------------------------------- */

class Settings_model extends Model {

	/* ------------------------
	*	Get Setting Details
	* ------------------------ */
	public function getSettings(){

		$result = $this->query('
			SELECT username, ganalytics_id, company
			FROM tbl_user_details
			');
		return $result;

	}

	/* ------------------------
	*	Get Setting Details
	* ------------------------ */
	public function updateSettings($userid, $username, $ganalyticsid){

		$result = $this->execute('
			UPDATE tbl_user_details
			SET username = "'. $username .'", ganalytics_id = "'. $ganalyticsid .'"
			WHERE id  = "'. $userid .'"
			');

		return $result;

	}

}