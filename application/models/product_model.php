<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Product Management Model
*	Date: 7 Mar 2013
* ------------------------------------- */

class Product_model extends Model {

	/* ------------------------
	*	Get Products
	* ------------------------ */
	public function getProducts(){

		$result = $this->query('
			SELECT product_id, name, LEFT(description, 86) as description, type 
			FROM tbl_products
			ORDER BY name 
			');
		return $result;

	}

	/* ------------------------
	*	Get Single Product
	* ------------------------ */
	public function getSingleProduct($product_id){

		$result = $this->query('
			SELECT p.product_id, p.name, p.description, p.type, pd.size, pd.stock_amt, pd.price, pd.detail_id
			FROM tbl_products p
			LEFT JOIN tbl_product_details pd 
				ON p.product_id = pd.product_id
			WHERE p.product_id = "'. $product_id .'"
			');

		return $result;

	}

	/* ------------------------
	*	Update Variance Product
	* ------------------------ */
	public function updateVariance($detail_id, $size, $stock, $price){

		$result = $this->execute('
			UPDATE tbl_product_details
			SET size = "'. $size .'", price = "'. $price .'", stock_amt = "'. $stock .'"
			WHERE detail_id  = "'. $detail_id .'"
			');

		return $result;
	}

	/* ------------------------
	*	Add New Variance Product
	* ------------------------ */
	public function addVariance($product_id, $size, $stock, $price){

		$result = $this->execute('
			INSERT INTO tbl_product_details (product_id, stock_amt, price, size)
			VALUES ("'. $product_id .'","'. $stock .'","'. $price .'","'. $size .'")
			');

		return $result;
	}

	/* ------------------------
	*	Delete Variance Product
	* ------------------------ */
	public function deleteVariance($delete_id){

		$result = $this->execute('
			DELETE FROM tbl_product_details
			WHERE detail_id = "'. $delete_id .'"
			');

		return $result;
	}

}
