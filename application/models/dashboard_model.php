<?php

/* ------------------------------------
*	@author: Ben Oakenfull	
*	Purpose: Order Management Model
*	Date: 29 Mar 2013
* ------------------------------------- */

class Dashboard_model extends Model {

	/* ------------------------
	*	Get Dashboard Data
	* ------------------------ */
	public function getDashboard(){

		$result = $this->query('
			SELECT TRUNCATE(SUM(order_amount),2) AS total_orders,
				(
					SELECT COUNT(stock_amt) AS stock
					FROM tbl_product_details
					WHERE stock_amt = 0
				) AS out_stock,
				(
					SELECT COUNT(fulfilled) AS fulfilled
					FROM tbl_orders
					WHERE fulfilled = 0
				) AS open_orders,
				(
					SELECT COUNT(order_id) AS order_count
					FROM tbl_orders
					WHERE DATE(shiped_date) = DATE(NOW())
  				) AS orders_today
			FROM tbl_orders
			WHERE DATE(shiped_date) = DATE(NOW())
			');
		return $result;

	}
}