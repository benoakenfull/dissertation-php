<?php

function pip()
{
	global $config;
    
    // Set our defaults
    $controller = $config['default_controller'];
    $action = 'index';
    $url = '';
	
	// Get request url and script url
	$request_url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
	$script_url  = (isset($_SERVER['PHP_SELF'])) ? $_SERVER['PHP_SELF'] : '';

	// Get our url path and trim the / of the left and the right
	if($request_url != $script_url) $url = trim(preg_replace('/'. str_replace('/', '\/', str_replace('index.php', '', $script_url)) .'/', '', $request_url, 1), '/');
    
	$segments = explode('?' , $url);

	//print_r($segments);

	//Split the controller and action
	if(isset($segments[0]) && $segments[0] != '') $body = $segments[0];

	//Gets the query parameters
	if(isset($segments[1]) && $segments[1] != '') $parameters = $segments[1];

	//Split the body variable to get segments
	@$body_segments = explode('/', $body);

	if(isset($body_segments[0]) && $body_segments[0] != '') $controller = $body_segments[0];
	if(isset($body_segments[1]) && $body_segments[1] != '') $action = $body_segments[1];

	// // Split the url into segments
	// $segments = explode('/', $url);
		
	// // Do our default checks
	// if(isset($segments[0]) && $segments[0] != '') $controller = $segments[0];
	// if(isset($segments[1]) && $segments[1] != '') $action = $segments[1];

	
	// | Modification - splits url up when paraments are added in url after a '?'
	// * =============================================================================

	// $urlbreaker = substr($segments[0], "0", strpos($segments[0], "?"));

	// if($urlbreaker){
	// 	$controller = substr($segments[0], "0", strpos($segments[0], "?"));
	// }

	// echo $action;

	
	// Get our controller file
    $path = APP_DIR . 'controllers/' . $controller . '.php';
	if(file_exists($path)){
        require_once($path);
	} else {
        $controller = $config['error_controller'];
        require_once(APP_DIR . 'controllers/' . $controller . '.php');
	}
    
    // Check the action exists
    if(!method_exists($controller, $action)){
        $controller = $config['error_controller'];
        require_once(APP_DIR . 'controllers/' . $controller . '.php');
        $action = 'index';
    }
	
	// Create object and call method
	$obj = new $controller;
    die(call_user_func_array(array($obj, $action), array_slice($segments, 2)));
}

